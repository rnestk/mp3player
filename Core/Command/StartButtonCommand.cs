﻿using Mp3PlayerApp.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Mp3PlayerApp.Core.Command
{
    public class StartButtonCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            FileInfo fInfo = parameter as FileInfo;
            if (null == fInfo) return;
            var soundService = SoundService.Create();
            soundService.Play(fInfo);
        }
    }
}
