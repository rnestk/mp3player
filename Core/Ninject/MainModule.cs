﻿using Mp3PlayerApp.Model.Entities;
using Mp3PlayerApp.Service;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Core.Ninject
{
    public class MainModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConfigurationService>().To<ConfigurationService>().InSingletonScope();
            Bind<ISoundService>().To<SoundService>().InSingletonScope();
            Bind<IFileService>().To<FileService>().InSingletonScope();
            Bind<IId3TagService>().To<Id3TagService>().InSingletonScope();
        }
    }
}
