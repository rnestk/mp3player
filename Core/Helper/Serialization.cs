﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Mp3PlayerApp.Core.Helper
{
    
    class Serialization
    {
        public static void StdSerialize<TEntity>(string path, TEntity entity, XmlWriterSettings settings = null) where TEntity : new()
        {
            settings = settings ?? new XmlWriterSettings { Indent = true };
            DataContractSerializer serializer = new DataContractSerializer(typeof(TEntity));
            using (Stream stream = File.Create(path))
            {
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    serializer.WriteObject(writer, entity);
                }
            }

        }

        public static TEntity StdDeserialize<TEntity>(string path, XmlReaderSettings settings = null) where TEntity : new()
        {
            settings = settings ?? new XmlReaderSettings();
            DataContractSerializer serializer = new DataContractSerializer(typeof(TEntity));
            using (Stream stream = File.OpenRead(path))
            {
                using (XmlReader reader = XmlReader.Create(stream, settings))
                {
                    return (TEntity)serializer.ReadObject(reader);
                }

            }
        }

        public static byte[] BinarySerialize<TEntity>(TEntity entity) where TEntity : new()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(TEntity));
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, entity);
                return stream.ToArray();
            }

        }


        public static TEntity BinaryDeserialize<TEntity>(byte[] bytes)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(TEntity));
            using (MemoryStream stream = new MemoryStream(bytes))
            {
                return (TEntity)serializer.ReadObject(stream);
            }
        }

        public static TEntity DeepClone<TEntity>(TEntity entity) where TEntity : new()
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(TEntity));
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, entity);
                stream.Position = 0;
                return (TEntity)serializer.ReadObject(stream);
            }
        }

    }
}
