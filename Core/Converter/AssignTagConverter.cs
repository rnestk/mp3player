﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Mp3PlayerApp.Core.Converter
{
    public class AssignTagConverter : IValueConverter
    {
        private FileInfo _latestValid;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FileInfo info = value as FileInfo;
            if(null == info)
            {
                return _latestValid ?? DependencyProperty.UnsetValue;
            }
            _latestValid = info;
            return info;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return _latestValid ?? DependencyProperty.UnsetValue;
        }
    }
}
