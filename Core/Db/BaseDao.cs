﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace Mp3PlayerApp
{
    public class BaseDao<TEntity> : IDisposable, IBaseDao<TEntity> where TEntity : BaseEntity, new()
    {

        protected DbContext _db;

        public Action<TEntity> BeforeAdd;
        public Action<TEntity, int> AfterAdd;
        public Action<TEntity, TEntity> BeforeUpdate;
        public Action<TEntity, int> AfterUpdate;
        public Action<TEntity, bool> BeforeDelete;
        public Action<TEntity, int, bool> AfterDelete;
        public Action<TEntity, TEntity> BeforeAddOrUpdate;
        public Action<TEntity, int> AfterAddOrUpdate;

        public BaseDao(DbContext db)
        {
            _db = db;
        }

        protected virtual IQueryable<TEntity> FilteredList()
        {
            return _db.Set<TEntity>().Where(e => !e.IsRemoved);
        }

        public virtual TEntity GetById(string id)
        {
            return _db.Set<TEntity>().FirstOrDefault(e => e.RowGuid == id);
        }

        public virtual TEntity GetById(int id)
        {
            return _db.Set<TEntity>().FirstOrDefault(e => e.Id == id);
        }

        public virtual async Task<TEntity> GetByIdAsync(string id)
        {
            return await Task.Run(() => 
            {
                return _db.Set<TEntity>().FirstOrDefault(e => e.RowGuid == id);
            });
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await Task.Run(() =>
            {
                return _db.Set<TEntity>().FirstOrDefault(e => e.Id == id);
            });
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return FilteredList();
        }

        public virtual IQueryable<TEntity> GetAllWithNonFiltered()
        {
            return _db.Set<TEntity>();
        }

        public virtual async Task<IQueryable<TEntity>> GetAllAsync()
        {
            return await Task.Run(() => 
            {
                return FilteredList();
            });
        }

        public virtual async Task<IQueryable<TEntity>> GetAllWithNonFilteredAsync()
        {
            return await Task.Run(() =>
            {
                return _db.Set<TEntity>();
            });
        }

        public virtual int Add(TEntity entity)
        {
            entity.DateCreated = DateTime.Now;
            entity.RowGuid = Guid.NewGuid().ToString();
            entity.IsActive = true;
            BeforeAdd?.Invoke(entity);
            TEntity added = _db.Set<TEntity>().Add(entity);
            int result = _db.SaveChanges();
            AfterAdd?.Invoke(added, result);
            return result;
        }

        public async virtual Task<int> AddAsync(TEntity entity)
        {
            return await Task.Run(() => 
            {
                entity.DateCreated = DateTime.Now;
                entity.RowGuid = Guid.NewGuid().ToString();
                entity.IsActive = true;
                BeforeAdd?.Invoke(entity);
                TEntity added = _db.Set<TEntity>().Add(entity);
                int result = _db.SaveChanges();
                AfterAdd?.Invoke(added, result);
                return result;
            });
        }

        public virtual int Update(TEntity entity)
        {
            TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            BeforeUpdate?.Invoke(original, entity);
            _db.Entry(original).CurrentValues.SetValues(entity);
            int result =_db.SaveChanges();
            AfterUpdate?.Invoke(entity, result);
            return result;
        }


        public virtual async Task<int> UpdateAsync(TEntity entity)
        {
            return await Task.Run(() => 
            {
                TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                BeforeUpdate?.Invoke(original, entity);
                _db.Entry(original).CurrentValues.SetValues(entity);
                int result = _db.SaveChanges();
                AfterUpdate?.Invoke(entity, result);
                return result;
            });
        }

        public virtual int AddOrUpdate(TEntity entity)
        {
            TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            int result = 0;
            if (null == original)
            {
                entity.DateCreated = DateTime.Now;
                entity.RowGuid = Guid.NewGuid().ToString();
                entity.IsActive = true;
                BeforeAddOrUpdate?.Invoke(original, entity);
                _db.Set<TEntity>().Add(entity);
                result = _db.SaveChanges();
                AfterAddOrUpdate?.Invoke(entity, result);
            }
            else
            {
                entity.DateModified = DateTime.Now;
                BeforeAddOrUpdate?.Invoke(original, entity);
                _db.Entry(original).CurrentValues.SetValues(entity);
                result = _db.SaveChanges();
                AfterAddOrUpdate?.Invoke(entity, result);
            }
            return result;
        }

        public virtual async Task<int> AddOrUpdateAsync(TEntity entity)
        {
            return await Task.Run(() =>
            {
                TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
                int result = 0;
                if (null == original)
                {
                    entity.DateCreated = DateTime.Now;
                    entity.RowGuid = Guid.NewGuid().ToString();
                    entity.IsActive = true;
                    BeforeAddOrUpdate?.Invoke(original, entity);
                    _db.Set<TEntity>().Add(entity);
                    result = _db.SaveChanges();
                    AfterAddOrUpdate?.Invoke(entity, result);
                }
                else
                {
                    entity.DateModified = DateTime.Now;
                    BeforeAddOrUpdate?.Invoke(original, entity);
                    _db.Entry(original).CurrentValues.SetValues(entity);
                    result = _db.SaveChanges();
                    AfterAddOrUpdate?.Invoke(entity, result);
                    
                }
                return result;
            });
        }

        public virtual int Deactivate(TEntity entity)
        {
            TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            entity.IsActive = false;
            _db.Entry(original).CurrentValues.SetValues(entity);
            return _db.SaveChanges();
        }

        public virtual async Task<int> DeactivateAsync(TEntity entity)
        {
            return await Task.Run(() =>
            {
                TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                entity.IsActive = false;
                _db.Entry(original).CurrentValues.SetValues(entity);
                return _db.SaveChanges();
            });
        }

        public virtual int Delete(TEntity entity)
        {
            TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            entity.DateModified = DateTime.Now;
            entity.IsActive = false;
            entity.IsRemoved = true;
            BeforeDelete?.Invoke(entity, false);
            _db.Entry(original).CurrentValues.SetValues(entity);
            int result = _db.SaveChanges();
            AfterDelete?.Invoke(entity, result, false);
            return result;
        }

        public virtual async Task<int> DeleteAsync(TEntity entity)
        {
            return await Task.Run(() =>
            {
                TEntity original = _db.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
                entity.DateModified = DateTime.Now;
                entity.IsActive = false;
                entity.IsRemoved = true;
                BeforeDelete?.Invoke(entity, false);
                _db.Entry(original).CurrentValues.SetValues(entity);
                int result = _db.SaveChanges();
                AfterDelete?.Invoke(entity, result, false);
                return result;
            });
        }

        public virtual int DeletePernament(TEntity entity)
        {
            BeforeDelete?.Invoke(entity, true);
            _db.Set<TEntity>().Remove(entity);
            int result = _db.SaveChanges();
            AfterDelete?.Invoke(entity, result, true);
            return result;
        }

        public virtual async Task<int> DeletePernamentAsync(TEntity entity)
        {
            return await Task.Run(() => 
            {
                BeforeDelete?.Invoke(entity, true);
                _db.Set<TEntity>().Remove(entity);
                int result = _db.SaveChanges();
                AfterDelete?.Invoke(entity, result, true);
                return result;
            });
        }

        public virtual TEntity ModelToModel(TEntity dest, TEntity source)
        {
            Type sourceType = source.GetType();
            Type destType = dest.GetType();

            PropertyInfo[] destinfo = destType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);
            PropertyInfo[] sourceInfo = sourceType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

            for (int i = 0; i < destinfo.Length; i++)
            {
                if (destinfo[i].CanWrite && destinfo[i].GetSetMethod(true) != null)
                {
                    string name = destinfo[i].Name;
                    var property = sourceInfo.Where(s => s.Name == name).FirstOrDefault();
                    if (null != property)
                    {
                        var value = property.GetValue(source, null);
                        if (sourceInfo[i] != null && null != value)
                        {
                            destinfo[i].SetValue(dest, Convert.ChangeType(value, destinfo[i].GetValue(dest, null).GetType(), null));
                        }
                    }


                }
            }

            return dest;
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}