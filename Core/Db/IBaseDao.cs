﻿using System.Linq;
using System.Threading.Tasks;

namespace Mp3PlayerApp
{
    public interface IBaseDao<TEntity> where TEntity : BaseEntity, new()
    {
        int Add(TEntity entity);
        Task<int> AddAsync(TEntity entity);
        int AddOrUpdate(TEntity entity);
        Task<int> AddOrUpdateAsync(TEntity entity);
        int Deactivate(TEntity entity);
        Task<int> DeactivateAsync(TEntity entity);
        int Delete(TEntity entity);
        Task<int> DeleteAsync(TEntity entity);
        int DeletePernament(TEntity entity);
        Task<int> DeletePernamentAsync(TEntity entity);
        void Dispose();
        IQueryable<TEntity> GetAll();
        Task<IQueryable<TEntity>> GetAllAsync();
        IQueryable<TEntity> GetAllWithNonFiltered();
        Task<IQueryable<TEntity>> GetAllWithNonFilteredAsync();
        TEntity GetById(int id);
        TEntity GetById(string id);
        Task<TEntity> GetByIdAsync(int id);
        Task<TEntity> GetByIdAsync(string id);
        TEntity ModelToModel(TEntity dest, TEntity source);
        int Update(TEntity entity);
        Task<int> UpdateAsync(TEntity entity);
    }
}