﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;

namespace Mp3PlayerApp.Service
{
    public interface IFileService
    {
        IList<TreeViewItem> FileList { get; }
        IList<TreeViewItem> FlatFileList { get; }
        Action<FileInfo> OnItemDoubleClick { get; set; }
        Action<FileInfo> OnItemSingleClick { get; set; }
    }
}