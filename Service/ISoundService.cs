﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Threading;

namespace Mp3PlayerApp.Service
{
    public interface ISoundService
    {
        MediaPlayer MediaPlayer { get; }
        Action<FileInfo> OnMediaEnded { get; set; }
        Action<FileInfo> OnMediaLoaded { get; set; }
        bool RepeatAll { get; set; }
        bool RepeatOne { get; set; }
        DispatcherTimer Timer { get; set; }

        void Pause();
        void Play(FileInfo info);
        void Stop();
    }
}