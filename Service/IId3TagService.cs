﻿using System.Collections.Generic;
using System.IO;
using Mp3PlayerApp.Model.Entities;

namespace Mp3PlayerApp.Service
{
    public interface IId3TagService
    {
        IList<Mp3File> Mp3Files { get; }
        void ResolveTags(FileInfo info);
    }
}