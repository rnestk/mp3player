﻿using Mp3PlayerApp.Model.Entities;

namespace Mp3PlayerApp.Service
{
    public interface IConfigurationService
    {
        Configuration Load();
        void Store(Configuration configuration);
    }
}