﻿using Mp3PlayerApp.Model.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Mp3PlayerApp.Service
{
    public class FileService : IFileService
    {
        private Configuration _configuration;
        public virtual IList<TreeViewItem> FileList
        {
            get
            {
                return GetFiles();
            }
           

        }

        public virtual IList<TreeViewItem> FlatFileList { get; protected set; }

        public FileService()
        {
            FlatFileList = new List<TreeViewItem>();
        }

        public FileService(Configuration configuration)
        {
            FlatFileList = new List<TreeViewItem>();
            _configuration = configuration;
        }

        public Action<FileInfo> OnItemDoubleClick { get; set; }
        public Action<FileInfo> OnItemSingleClick { get; set; }

        protected virtual IList<TreeViewItem> GetFiles()
        {
            var list = new List<TreeViewItem>();

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                TreeViewItem item = new TreeViewItem();
                
                item.Header = drive.RootDirectory;
                string[] dirs = Directory.GetDirectories(drive.RootDirectory.ToString());
                string[] files = Directory.GetFiles(drive.RootDirectory.ToString());
                FlatFileList.Add(item);
                AddNodes(item, dirs, files);
                list.Add(item);

                ItemDecorate(item, drive.RootDirectory);
            }

            return list;
        }

        protected virtual void ItemDecorate(TreeViewItem item, DirectoryInfo info)
        {
            //StackPanel stack = new StackPanel();
            //TextBlock tBlock = new TextBlock();
            //Rectangle rectangle = new Rectangle();
            //SolidColorBrush color = new SolidColorBrush();

            //color.Color = Color.FromRgb(0, 0, 0);
            //rectangle.Fill = color;
            //rectangle.Width = 10; rectangle.Height = 10;

            //tBlock.Text = info.Name;

            //stack.Orientation = Orientation.Horizontal;
            //stack.Children.Add(rectangle);
            //stack.Children.Add(tBlock);

            //item.Header = stack;

            item.Header = info.Name;
        }

        protected virtual void AddNodes(TreeViewItem item, string[] dirs, string[] files)
        {
            
            if (dirs.Any())
            {
                foreach (var dir in dirs)
                {
                    TreeViewItem newItem = new TreeViewItem();
                    DirectoryInfo di = new DirectoryInfo(dir);
                    FileAttributes attrs = di.Attributes;
                    if((attrs & FileAttributes.System) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Temporary) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Device) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Encrypted) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Hidden) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Compressed) != 0)
                    {
                        continue;
                    }
                    string[] subdirs;
                    string[] subfiles;
                    try
                    {
                        
                        subdirs = Directory.GetDirectories(di.FullName);
                        subfiles = Directory.GetFiles(di.FullName);
                    }
                    catch (UnauthorizedAccessException e) { continue; };
                    newItem.Tag = di;
                    ItemDecorate(newItem, di);
                    newItem.Expanded += (obj, re) => 
                    {
                        TreeViewItem current = obj as TreeViewItem;
                        if(null != current)
                        {
                            DirectoryInfo info = current.Tag as DirectoryInfo;
                            if(null != info)
                            {
                                TreeViewItem todelete = (TreeViewItem)newItem.Items.OfType<TreeViewItem>().Where(e => e.Header == null).FirstOrDefault();
                                if(null != todelete) newItem.Items.Remove(todelete);
                                if(!newItem.HasItems)
                                {
                                    AddNodes(newItem, Directory.GetDirectories(di.FullName), Directory.GetFiles(di.FullName));
                                }
                               
                            }
                        }
                        
                    }; 

                    newItem.MouseDown += (obj, re) =>
                    {
                        TreeViewItem current = obj as TreeViewItem;
                        
                        if (null != current)
                        {
                            FileInfo info = current.Tag as FileInfo;
                            if (null != info)
                            {

                            }
                        }
                    };

                    if (subdirs.Any() || subfiles.Any())
                    {
                        newItem.Items.Add(new TreeViewItem { Header = null });
                    }
                    FlatFileList.Add(newItem);
                    
                    item.Items.Add(newItem);                    
                }
                
            }
           

            if(files.Any())
            {
                foreach (var file in files)
                {
                    FileAttributes attrs = File.GetAttributes(file);
                    if ((attrs & FileAttributes.System) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Temporary) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Device) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Encrypted) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Hidden) != 0)
                    {
                        continue;
                    }
                    if ((attrs & FileAttributes.Compressed) != 0)
                    {
                        continue;
                    }

                    TreeViewItem newItem = new TreeViewItem();
                    
                    FileInfo info = new FileInfo(file);
                    if (info.Extension.ToLower() != ".mp3") continue;
                    newItem.Header = info.Name;
                    newItem.Tag = info;

                    if(info.FullName == _configuration.TrackPath)
                    {
                        newItem.IsSelected = true;
                    }

                    newItem.MouseDoubleClick += (obj, re) =>
                    {
                        TreeViewItem current = obj as TreeViewItem;
                        if (null != current)
                        {
                            FileInfo finfo = current.Tag as FileInfo;
                            if (null != finfo)
                            {
                                OnItemDoubleClick?.Invoke(finfo);
                            }
                        }
                    };

                    newItem.MouseDown += (obj, re) =>
                    {
                        TreeViewItem current = obj as TreeViewItem;
                        if (null != current)
                        {
                            FileInfo finfo = current.Tag as FileInfo;
                            if (null != finfo)
                            {
                                OnItemSingleClick?.Invoke(finfo);
                            }
                        }
                    };

                    FlatFileList.Add(newItem);
                    item.Items.Add(newItem);

                }
            }

            

        }

       
    }
}
