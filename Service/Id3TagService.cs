﻿using Mp3PlayerApp.Model.Entities;
using Shell32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Service
{
    public class Id3TagService : IId3TagService
    {
        public IList<Mp3File> _mp3Files;
        private FileInfo _current;
        private Shell sh32;

        public Id3TagService()
        {
            _mp3Files = new List<Mp3File>();
            sh32 = new Shell();
        }

        public IList<Mp3File> Mp3Files
        {
            get { return _mp3Files; }
            protected set { _mp3Files = value; }
        }


        public void ResolveTags(FileInfo info)
        {
            if (null == info) return;
            if (Object.Equals(_current, info)) return;
            _mp3Files.Clear();
            DirectoryInfo dInfo = info.Directory;
            IList<FileInfo> infos = dInfo.GetFiles("*.mp3");
            if(infos.Any())
            {
                int number = 1;
                foreach (var item in infos)
                {
                    Mp3File newFile = RetriveFileTags(item);
                    newFile.Id = number;
                    _mp3Files.Add(newFile);
                    number++;
                }
                _current = info;
            }

            
        }

        protected Mp3File RetriveFileTags(FileInfo info)
        {
            Folder dir = sh32.NameSpace(info.DirectoryName);
            FolderItem file = dir.ParseName(info.Name);
            DateTime dateCreated = new DateTime();
            DateTime dateModified = new DateTime();
            int id = 0;
            TimeSpan length = TimeSpan.Zero;
            double size = 0;
            int year = 0;
            Mp3File mp3file = new Mp3File();

            mp3file.Title = dir.GetDetailsOf(file, 0);
            mp3file.AlbumTitle = dir.GetDetailsOf(file, 14);
            mp3file.Artist = dir.GetDetailsOf(file, 13);
            mp3file.FileType = dir.GetDetailsOf(file, 2);
            mp3file.Genre = dir.GetDetailsOf(file, 16);
            mp3file.Id = id;

            if (null != dir.GetDetailsOf(file, 4))
            {
                bool parsed = DateTime.TryParse(dir.GetDetailsOf(file, 4), out dateCreated);
                if(parsed)
                {
                    mp3file.DateCreated = dateCreated;
                }
                
            }

            if (null != dir.GetDetailsOf(file, 3))
            {
                bool parsed = DateTime.TryParse(dir.GetDetailsOf(file, 3), out dateModified);
                if (parsed)
                {
                    mp3file.DateModified = dateModified;
                }

            }

            if (null != dir.GetDetailsOf(file, 27))
            {
                bool parsed = TimeSpan.TryParse(dir.GetDetailsOf(file, 27), out length);
                if (parsed)
                {
                    mp3file.Length = length;
                }

            }

            if (null != dir.GetDetailsOf(file, 1))
            {
                bool parsed = double.TryParse(dir.GetDetailsOf(file, 1), out size);
                if (parsed)
                {
                    mp3file.Size = size;
                }

            }

            if (null != dir.GetDetailsOf(file, 15))
            {
                bool parsed = int.TryParse(dir.GetDetailsOf(file, 15), out year);
                if (parsed)
                {
                    mp3file.Year = year;
                }

            }

            return mp3file;

        }

        
    }
}
