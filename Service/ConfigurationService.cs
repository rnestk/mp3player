﻿using Mp3PlayerApp.Core.Helper;
using Mp3PlayerApp.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Service
{
    public class ConfigurationService : INotifyPropertyChanged, IConfigurationService
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string AppFolder { get; protected set; }
        public static readonly string AppTitle = "Mp3 Commander";
        protected string _configurationFileName;
        protected static ConfigurationService _instance;
        private static object _locker = new object();
        private string[] _allowedExtensions;

        public ConfigurationService()
        {
            AppFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppTitle);
            _configurationFileName = "configuration.xml";
            _instance = this;
        }

        public static ConfigurationService Create()
        {
            if(null == _instance)
            {
                lock(_locker)
                {
                    _instance = new ConfigurationService();
                }
                
            }
            return _instance;
        }

       

        public virtual void Store(Configuration configuration) 
        {
            InitAppFolder();
            Serialization.StdSerialize(Path.Combine(AppFolder, _configurationFileName), configuration);
        }

        public virtual Configuration Load() 
        {
            InitAppFolder();
            InitConfigurationFile();
            return Serialization.StdDeserialize<Configuration>(Path.Combine(AppFolder, _configurationFileName));
        }

        private bool InitAppFolder()
        {
            if(!Directory.Exists(Path.Combine(AppFolder, _configurationFileName)))
            {
                Directory.CreateDirectory(AppFolder);
                return true;
            }
            return false;
        }

        private bool InitConfigurationFile()
        {
            if(!File.Exists(Path.Combine(AppFolder, _configurationFileName)))
            {
                Serialization.StdSerialize(Path.Combine(AppFolder, _configurationFileName), new Configuration());
                return true;
            }
            return false;
        }

    }
}
