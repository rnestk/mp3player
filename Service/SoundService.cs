﻿using Mp3PlayerApp.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Threading;

namespace Mp3PlayerApp.Service
{
    public class SoundService : INotifyPropertyChanged, ISoundService
    {
        private MediaPlayer _mp;

        private TimeSpan _currentPosition;
        public DispatcherTimer Timer { get; set; }
        public MediaPlayer MediaPlayer { get { return _mp; } }
        private static SoundService _instance;
        private static object _locker = new object();
        public event PropertyChangedEventHandler PropertyChanged;
        public Action<FileInfo> OnMediaEnded { get; set; }
        public Action<FileInfo> OnMediaLoaded { get; set; }
        public bool RepeatOne { get; set; }
        public bool RepeatAll { get; set; }
        private int _index;
        private IList<FileInfo> _mp3Files;
        private static Configuration _configuration;
        
        public SoundService(Configuration configuration)
        {
            _mp = new MediaPlayer();
            Timer = new DispatcherTimer();
            _configuration = configuration;
            _instance = this;
            _mp.MediaOpened += (obj, e) => {
                
            };
            _currentPosition = TimeSpan.Zero;
        }

        public static SoundService Create()
        {
            lock (_locker)
            {

                if (null == _instance)
                {
                    _instance = new SoundService(_configuration);
                }
            }
            return _instance;
        }

        public void Play(FileInfo info)
        {
            if (info.Extension.ToLower() != ".mp3") return;
            string dir = Path.GetDirectoryName(info.FullName);
            DirectoryInfo dinfo = new DirectoryInfo(dir);
            _mp3Files = dinfo.GetFiles("*.mp3").ToList();
            FileInfo selected = _mp3Files.Where(e => e.FullName == info.FullName).FirstOrDefault();
            _index = _mp3Files.IndexOf(selected);
            _mp.MediaEnded -= OnTrackEnded;
            _mp.MediaEnded += OnTrackEnded;
            _configuration.TrackPath = info.FullName;
            _mp.Open(new Uri(info.FullName));
            _mp.Position = _currentPosition;
            _mp.Play();
            Timer.Start();
            _currentPosition = TimeSpan.Zero;
            var duration = _mp.NaturalDuration.ToString();

            
        }

        private int HandleIndex(int index, int totalCount)
        {
            if(RepeatAll)
            {
                if(index == totalCount - 1)
                {
                    return 0;
                }
                return ++index;
            }
            if(RepeatOne)
            {
                return index;
            }
            return ++index;
        }

        public void Stop()
        {
            _mp.Stop();
            Timer.Stop();
            _currentPosition = TimeSpan.Zero;
        }

        private void OnTrackEnded(object o, EventArgs e)
        { 

            _index = HandleIndex(_index, _mp3Files.Count);
            if (_index < _mp3Files.Count)
            {
                OnMediaEnded?.Invoke(_mp3Files[_index]);
                _mp.Open(new Uri(_mp3Files[_index].FullName));
                _mp.Play();
                _configuration.TrackPath = _mp3Files[_index].FullName;
            }
            else
            {
                _mp.Stop();
                OnMediaEnded?.Invoke(_mp3Files[_mp3Files.Count - 1]);
                _index = 0;
                //_configuration.TrackPath = _mp3Files[_mp3Files.Count - 1].FullName;
            }
            
        }

        public void Pause()
        {
            _mp.Pause();
            _currentPosition = _mp.Position;
           
        }

    }
}
