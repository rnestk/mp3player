﻿using Mp3PlayerApp.Model.Entities;
using Mp3PlayerApp.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace Mp3PlayerApp.ViewModel
{
    public class MainViewModel : ObservableCollection<Mp3File>, INotifyPropertyChanged
    {
        private IFileService _fileService;
        private ISoundService _soundService;
        private Configuration _configuration;
        private string _trackLength;
        private int _trackSlider { get; set; }
        private int _trackMax;
        private int _trackMaxInit;
        private int _trackMaxGrade;
        private ComboBoxElement _selectedComboBoxElement;
        private IList<ComboBoxElement> _comboBoxElementList;
        private IId3TagService _tagService;
        private ObservableCollection<Mp3File> _mp3FilesInfo;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            _fileService =  null;
            _soundService = null;
            _trackMaxInit = 100;
            _trackMaxGrade = 1000;

            _fileService.OnItemDoubleClick += (fi) =>
            {
                _soundService.Play(fi);
            };
            _soundService.OnMediaEnded += (info) =>
            {
                TreeViewItem next = _fileService.FlatFileList.Where(e => e.Header?.ToString() == info.Name).FirstOrDefault();
                if (null != next)
                {
                    next.IsSelected = true;
                }
            };

            TrackMax = _trackMaxInit;

            _soundService.Timer.Interval = TimeSpan.FromMilliseconds(50);
            _soundService.Timer.Tick += (sender, args) =>
            {
                if (_soundService.MediaPlayer.Source != null)
                {
                    if (_soundService.MediaPlayer.NaturalDuration.HasTimeSpan)
                    {
                        var duration = _soundService.MediaPlayer.NaturalDuration.TimeSpan;
                        var position = _soundService.MediaPlayer.Position;
                        TrackInfo = string.Format("{0:00}:{1:00}:{2:00} / {3:00}:{4:00}:{5:00}", position.Hours, position.Minutes, position.Seconds, duration.Hours, duration.Minutes, duration.Seconds);
                        TrackMax = (int)_soundService.MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds * _trackMaxGrade;
                        TrackSlider = (int)(position.TotalSeconds * _trackMaxGrade);

                    }
                    else TrackMax = _trackMaxInit * _trackMaxGrade;

                }
                else TrackMax = _trackMaxInit * _trackMaxGrade;
            };
        }

        public MainViewModel(IFileService fileService, ISoundService soundService, IId3TagService tagService, Configuration configuration)
        {
            _fileService = fileService;
            _soundService = soundService;
            _configuration = configuration;
            _trackMaxInit = 100;
            _trackMaxGrade = 1000;
            _comboBoxElementList = ComboBoxElementList();
            _selectedComboBoxElement = _comboBoxElementList[0];
            _tagService = tagService;

            _fileService.OnItemDoubleClick += (fi) =>
            {
                _soundService.Play(fi);
            };
            _soundService.OnMediaEnded += (info) =>
            {
                TreeViewItem next = _fileService.FlatFileList.Where(e => e.Header?.ToString() == info.Name).FirstOrDefault();
                if (null != next)
                {
                    next.IsSelected = true;

                }
            };

            TrackMax = _trackMaxInit;

            _soundService.Timer.Interval = TimeSpan.FromMilliseconds(50);
            _soundService.Timer.Tick += (sender, args) =>
            {
                if (_soundService.MediaPlayer.Source != null)
                {
                    if (_soundService.MediaPlayer.NaturalDuration.HasTimeSpan)
                    {
                        var duration = _soundService.MediaPlayer.NaturalDuration.TimeSpan;
                        var position = _soundService.MediaPlayer.Position;
                        TrackInfo = string.Format("{0:00}:{1:00}:{2:00} / {3:00}:{4:00}:{5:00}", position.Hours, position.Minutes, position.Seconds, duration.Hours, duration.Minutes, duration.Seconds);
                        TrackMax = (int)_soundService.MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds * _trackMaxGrade;
                        TrackSlider = (int)(position.TotalSeconds * _trackMaxGrade);

                    }
                    else TrackMax = _trackMaxInit * _trackMaxGrade;

                }
                else TrackMax = _trackMaxInit * _trackMaxGrade;
            };
        }

        protected virtual TreeViewItem FindNode(IList<TreeViewItem> fileList, FileInfo info)
        {
            var list = fileList.SelectMany(e => e.Items.OfType<TreeViewItem>());
            foreach (TreeViewItem item in list)
            {
                if (item.Header?.ToString() == info.Name) return item;
                return FindNode(item.Items.OfType<TreeViewItem>().ToList(), info);
            }
            return null;


        }

        public IList<TreeViewItem> FileList
        {
            get
            {
                _fileService.OnItemDoubleClick += (info) =>
                {
                    _tagService.ResolveTags(info);
                    Mp3FilesInfo = ToObservableCollection(_tagService.Mp3Files);
                };
                return _fileService.FileList;
            }
        }

        private ObservableCollection<Mp3File> ToObservableCollection(IList<Mp3File> mp3Files)
        {
            ObservableCollection<Mp3File> oc = new ObservableCollection<Mp3File>();
            foreach (var item in mp3Files)
            {
                oc.Add(item);
            }
            return oc;
        }

        public string TrackInfo
        {
            get { return _trackLength; }
            set
            {

                _trackLength = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TrackInfo"));
            }
        }

        public int TrackSlider
        {
            get
            {
                return _trackSlider;

            }
            set
            {
                _trackSlider = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TrackSlider"));
            }
        }

        public int TrackMax
        {
            get { return _trackMax; }
            protected set
            {
                _trackMax = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TrackMax"));
            }
        }

        public ComboBoxElement SelectedComboBoxElement
        {
            get
            {
                return _selectedComboBoxElement;
            }
            set
            {
                _selectedComboBoxElement = value;
                if(null != value)
                {
                    string tag = value.Name.ToString();
                    switch (tag.ToUpper())
                    {
                        case "REPEATOFF":
                            _soundService.RepeatAll = false;
                            _soundService.RepeatOne = false;
                            _configuration.RepeatOption = "repeatOff";
                            break;
                        case "REPEATALL":
                            _soundService.RepeatAll = true;
                            _soundService.RepeatOne = false;
                            _configuration.RepeatOption = "repeatAll";
                            break;
                        case "REPEATONE":
                            _soundService.RepeatAll = false;
                            _soundService.RepeatOne = true;
                            _configuration.RepeatOption = "repeatOne";
                            break;
                        default:
                            _soundService.RepeatAll = false;
                            _soundService.RepeatOne = false;
                            _configuration.RepeatOption = "repeatOff";
                            break;
                    }
                }
            }
        }

        public int Volume
        {
            get
            {
                double vol = _soundService.MediaPlayer.Volume * 100;
                int res = (int)Math.Round(vol);
                return (int)vol;
            }

            set
            {
                double val = value;
                _soundService.MediaPlayer.Volume = (val / 100);
            }
        }

        public IList<ComboBoxElement> ComboBoxElements
        {
            get
            {
                return _comboBoxElementList;
            }
        }

        public ObservableCollection<Mp3File> Mp3FilesInfo
        {
            get
            {
                return _mp3FilesInfo;
            }
            set
            {
                _mp3FilesInfo = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mp3FilesInfo"));
            }
           
        }

        
        private IList<ComboBoxElement> ComboBoxElementList()
        {
            return new List<ComboBoxElement>
                {
                    new ComboBoxElement
                    {
                        IsSelected = true,
                        Tag = "REPEATOFF",
                        Name = "repeatOff",
                        Content = "Powtarzanie wyłączone",
                        ImagePath = @"/Content/ShutDown_16x.png"
                    },
                    new ComboBoxElement
                    {
                        IsSelected = false,
                        Tag = "REPEATONE",
                        Name = "repeatOne",
                        Content = "Powtórz 1 utwór",
                        ImagePath = @"/Content/Undo_grey_12x_16x.png"
                    },
                    new ComboBoxElement
                    {
                        IsSelected = false,
                        Tag = "REPEATALL",
                        Name = "repeatAll",
                        Content = "Powtórz folder",
                        ImagePath = @"/Content/All_16x.png"
                    },
                };
        }

    }
}
