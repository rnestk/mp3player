﻿using Mp3PlayerApp.Core.Ninject;
using Mp3PlayerApp.Model.Entities;
using Mp3PlayerApp.Service;
using Mp3PlayerApp.ViewModel;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Mp3PlayerApp
{
    
    public partial class App : Application
    {
        private IConfigurationService _configurationService;
        private Configuration _configuration;

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            _configurationService.Store(_configuration);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            

            IKernel kernel = new StandardKernel(new MainModule());
            
            _configurationService = kernel.Get<IConfigurationService>();
            _configuration = _configurationService.Load();
            kernel.Bind<Configuration>().ToConstant(_configuration).InSingletonScope();
            IFileService fileService = kernel.Get<IFileService>();
            ISoundService soundService = kernel.Get<ISoundService>();
            IId3TagService tagService = kernel.Get<IId3TagService>();
            var vm = new MainViewModel(fileService, soundService, tagService, _configuration);
            var window = new MainWindow
            {
                DataContext = vm,
                Title = ConfigurationService.AppTitle,
                Icon = new BitmapImage(new Uri("pack://application:,,,/Mp3PlayerApp;component/Content/SingleOutput_8170.ico"))
            };
            window.Show();
        }
    }
}
