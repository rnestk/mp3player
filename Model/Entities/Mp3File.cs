﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Model.Entities
{
    public class Mp3File
    {
        public int Id { get; set; }
        public double? Size { get; set; }
        public string FileType { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public string AlbumTitle { get; set; }
        public int? Year { get; set; }
        public string Artist { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public TimeSpan? Length { get; set; }
    }
}
