﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Model.Entities
{
    public class ComboBoxElement
    {
        public string Name { get; set; }
        public object Tag { get; set; }
        public bool IsSelected { get; set; }
        public string Content { get; set; }
        public string ImagePath { get; set; }
    }
}
