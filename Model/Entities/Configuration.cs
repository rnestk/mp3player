﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Mp3PlayerApp.Model.Entities
{
    [DataContract]
    public class Configuration
    {
        private string _repeatOption;
        private string _trackPath;
        private double? _volume;
        [DataMember]
        public string TrackPath
        {
            get { return _trackPath; }
            set
            {
                _trackPath = value;
            }
        }
        [DataMember]
        public string RepeatOption
        {
            get { return _repeatOption; }
            set { _repeatOption = value; }
        }
        [DataMember]
        public double? Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
    }
}
