﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Mp3PlayerApp.Model.Entities
{
    public class TreeViewElement
    {
        private bool _hasItems;

        public int Id { get; set; }
        public object Header { get; set; }
        public object Tag { get; set; }
        public bool IsSelected { get; set; }
        public ItemCollection Items { get; set; }
        public RoutedEventHandler Expanded { get; set; }
        public MouseButtonEventHandler MouseDoubleClick { get; set; }
        public MouseButtonEventHandler MouseDown { get; set; }

        public bool HasItems
        {
            get { return _hasItems; }
            protected set
            {
                _hasItems = Items.Count > 0;
            }
        }

        public TreeViewElement()
        {
            Items = new DataGrid().Items;
        }

        //public void test()
        //{
        //    TreeViewItem it = new TreeViewItem();
        //    it.Expanded += null;
        //    it.MouseDoubleClick += null;
        //    it.MouseDown += null;
        //}
    }
}
